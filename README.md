

Steps.
1. Prepare running environment.  
	1.1 install JDK  
		Some computers already installed. Type below line in cmd to check your JDK.  
		java -version  
		If not installed, go to below url and install it. If you downloaded zip version instead of exe version, set up system variable.  
		https://www.oracle.com/technetwork/java/javase/downloads/index.html  
	1.2 Install Maven  
		Go to https://maven.apache.org/download.cgi, and download "Binary zip archive" and unzip it.  
		Set up your system variable for maven.  
 	1.3 Install git.(optional)  
		Go to url https://git-scm.com/downloads, downloads and install git.  
2. Download project from repository.  
    Click "Clone and Download" on this repository(https://github.ent.stateauto.com/StateAuto/QE-RAAPI-Nucleus), and Click "Download ZIP".  
    Or Clone using below steps if completed 1.3.  
	Open cmd, navigate to the folder you want to download the project.  
	Copy paste below line and hit enter.  
	git clone https://github.ent.stateauto.com/StateAuto/QE-RAAPI-Nucleus.git  
	If shows credential, please provide your credential;  
3. Build work space.   
	Go to data folder inside downloaded folder, and double click buildProject.bat.  
	After build, if you see some test in below file, the built successful.  
	%yourProjectFolder%->report->%currentTimeFolder%->report_%currentTime%.html  
4. Edit test data.  
   Open testData excel, edit and save it (testData location: %yourProjectFolder%->data->testData.xlsx).  
   Tips for edit test data.  
   When click on some cells in this excel, the cells input message pops up. Change cells based on that information.    
   Can be multiple test data sheet(but only one workbook), and name must start with "test_".  
5. Run your test.   
   Double click below batch file.  
   %yourProjectFolder%->data->runTest.bat  
   
Author: Abdullah 
   

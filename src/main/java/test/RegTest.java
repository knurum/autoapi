//Author Abdullah (abdullah.nurum@stateauto.com)
package test;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import api.APIController;
import library.BasePage;

@Listeners(library.TestNGListener.class)
public class RegTest extends BasePage {

	APIController apiController = new APIController();

	@AfterMethod(alwaysRun = true)
	public void AfterMethod() {
		infos.put("totalTest", (int) (infos.get("totalTest")) + 1);
		report.writeRecord(testData);
		excel.writeStatus(testData);
		testData = null;
	}

	@Test(dataProvider = "data")
	public void myTest(LinkedHashMap<String, Object> testDatas) {
		testData = testDatas;
		testData = excel.getTestData(testData);
		testData = apiController.restAssred(testData);
	}

	@DataProvider(name = "data")
	public Object[] dataprovider() {
		ArrayList<LinkedHashMap<String, Object>> testCases = excel.getTestCases((String[]) infos.get("tags"),((String[]) infos.get("runSelection"))[0]);
		Object[] array = new Object[testCases.size()];
		return testCases.toArray(array);

	}

}

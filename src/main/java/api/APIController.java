//Author Abdullah (abdullah.nurum@stateauto.com)
package api;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;

import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import library.BasePage;
import library.Util;

public class APIController extends Matchers {

	private Util util = BasePage.util;

	public LinkedHashMap<String, Object> restAssred(LinkedHashMap<String, Object> testData) {

		String requestFile = util.createTempFile("request");
		String responseFile = util.createTempFile("response");
		PrintStream requestLogStream = util.createLogStream(requestFile);
		PrintStream responseLogStream = util.createLogStream(responseFile);
		RequestSpecification req = given();

		req = addFormParam(testData.get("formParam"), req);
		req = addAuth(testData.get("auth"), req, testData);
		req = addHeader(testData.get("header"), req);
		req = addQueryParam(testData.get("queryParam"), req);
		req = addFileUplad(testData.get("uploadFile"), req);
		req = encodeURL(testData.get("encodeURL"), req);
		req = addReqBody(testData.get("requestBody"), req);

		req.filter(RequestLoggingFilter.logRequestTo(requestLogStream));
		req.filter(ResponseLoggingFilter.logResponseTo(responseLogStream));
		testData.put("requestStartTime", Calendar.getInstance());

		Response resp = getResponse(req, util.getUrl(util.getStringVarible(testData.get("url"))),
				util.getStringVarible(testData.get("method")));
		testData = saveResponseFile(resp, testData.get("responseStreamFileName"), testData);

		testData.put("requestFinishedTime", Calendar.getInstance());
		testData.put("actualAtatusCode", resp.getStatusCode());

		String requestContent = util.getFileContent(requestFile);
		String responseContent = util.getFileContent(responseFile);
		testData.put("requestObject", requestContent);
		testData.put("responseObject", responseContent);
		ValidatableResponse validateResp = resp.then().assertThat();
		testData = checkCookie(validateResp, testData.get("cookie"), testData);
		testData = checkStatusCode(validateResp, testData.get("statusCode"), testData);
		testData = checkHeader(validateResp, testData.get("resHeader"), testData);
		testData = checkResponseContentType(validateResp, testData.get("responseContentType"), testData);
		testData = checkResponseBody(validateResp, testData.get("responseBody"), testData);
		testData = checkResponseSection(validateResp, testData.get("responseSection"), testData);

		return testData;
	}

	private LinkedHashMap<String, Object> saveResponseFile(Response resp, Object obj,
			LinkedHashMap<String, Object> testData) {

		String fileName = util.getStringVarible(obj);

		if (fileName != null) {
			String contentType = resp.contentType().trim().toLowerCase();
			if (contentType.equals("application/octet-stream")) {
				fileName = "report\\" + util.getFolderName() + "\\"+ fileName;
				InputStream stream = resp.asInputStream();
				File targetFile = new File(fileName);
				try {
					FileUtils.copyInputStreamToFile(stream, targetFile);
					stream.close();
					testData.put("fileUpload", "File Saved to folder.");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return testData;
	}

	private RequestSpecification addFormParam(Object obj, RequestSpecification req) {
		for (Entry<String, String> entry : util.getMapVarible(obj).entrySet()) {
			req = req.formParam(entry.getKey(), entry.getValue());
		}
		return req;
	}

	private RequestSpecification addFileUplad(Object obj, RequestSpecification req) {
		if (util.getMapVarible(obj).size() > 0) {
			req = req.contentType("multipart/form-data");
		}
		for (Entry<String, String> entry : util.getMapVarible(obj).entrySet()) {

			req = req.multiPart(entry.getKey(), (new File(entry.getValue())), "multipart/form-data");

		}
		return req;
	}

	private RequestSpecification addAuth(Object obj, RequestSpecification req, LinkedHashMap<String, Object> testData) {

		String auth = util.getStringVarible(obj);
		if (auth != null) {
			JsonPath jsonPath = new JsonPath(auth);
			String username = jsonPath.getString("username");
			String password = jsonPath.getString("password");
			if (username == null || password == null) {

				String sheet = (String) testData.get("sheet");
				String testNum = (String) testData.get("testNum");
				throw new NullPointerException(
						"JSON format has problem. Format should be {\"username\":\"yourusername\",\"password\":\"yourpassword\"} Please check your json on auth column in test data sheet. sheet Name: "
								+ sheet + ", row number: " + testNum);

			}
			req = req.auth().preemptive().basic(username, password);

		} else {
			req = req.urlEncodingEnabled(true);
		}
		return req;
	}

	private RequestSpecification encodeURL(Object obj, RequestSpecification req) {

		String encodeURL = util.getStringVarible(obj);
		if (encodeURL != null && encodeURL.trim().toLowerCase().equals("no")) {
			req = req.urlEncodingEnabled(false);
		} else {
			req = req.urlEncodingEnabled(true);
		}
		return req;
	}

	private LinkedHashMap<String, Object> checkResponseSection(ValidatableResponse validateResp, Object obj,
			LinkedHashMap<String, Object> testData) {
		LinkedHashMap<String, String> expected = util.getMapVarible(obj);
		for (Map.Entry<String, String> entry : expected.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			validateResp.body(key, equalTo(value));
			testData.put("checkResponseSection", "Response body section is as expected");
		}
		return testData;
	}

	private LinkedHashMap<String, Object> checkResponseBody(ValidatableResponse validateResp, Object obj,
			LinkedHashMap<String, Object> testData) {
		String expected = util.getStringVarible(obj);
		if (expected != null) {
			validateResp.body(equalTo(expected));
			testData.put("checkResponseBody", "Response body is as expected");
		}
		return testData;
	}

	private LinkedHashMap<String, Object> checkHeader(ValidatableResponse validateResp, Object obj,
			LinkedHashMap<String, Object> testData) {
		LinkedHashMap<String, String> expected = util.getMapVarible(obj);
		for (Map.Entry<String, String> entry : expected.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			validateResp.header(key, value);
			testData.put("checkHeader", "Response Header is as expected");
		}

		return testData;
	}

	private LinkedHashMap<String, Object> checkCookie(ValidatableResponse validateResp, Object obj,
			LinkedHashMap<String, Object> testData) {
		LinkedHashMap<String, String> expected = util.getMapVarible(obj);
		for (Map.Entry<String, String> entry : expected.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			validateResp.cookie(key, value);
			testData.put("checkCookie", "Response cookie is as expected");
		}

		return testData;
	}

	private LinkedHashMap<String, Object> checkResponseContentType(ValidatableResponse validateResp, Object obj,
			LinkedHashMap<String, Object> testData) {
		String expected = util.getStringVarible(obj);
		if (expected != null) {
			validateResp.contentType(expected);
			testData.put("checkResponseContentType", "Response Content-Type is as expected.");
		}
		return testData;
	}

	private LinkedHashMap<String, Object> checkStatusCode(ValidatableResponse validateResp, Object obj,
			LinkedHashMap<String, Object> testData) {
		String expected = util.getStringVarible(obj);

		if (expected != null) {
			validateResp.statusCode(util.stringToNum(expected));
			testData.put("checkStatusCode", "Status code is as expected");
		}
		return testData;
	}

	private Response getResponse(RequestSpecification req, String url, String method) {
		if (method == null) {
			method = "get";
		}
		Response resp = null;
		method = method.toLowerCase();
		switch (method) {
		case "post":
			resp = req.post(url);
			break;
		case "put":
			resp = req.put(url);
			break;
		case "delete":
			resp = req.delete(url);
			break;
		default:
			resp = req.get(url);
		}
		return resp;

	}

	private RequestSpecification addReqBody(Object obj, RequestSpecification req) {
		String body = util.getStringVarible(obj);
		if (body != null) {
			req = req.body(body);
		}
		return req;
	}

	private RequestSpecification addQueryParam(Object obj, RequestSpecification req) {
		for (Entry<String, String> entry : util.getMapVarible(obj).entrySet()) {
			req = req.queryParam(entry.getKey(), entry.getValue());
		}
		return req;
	}

	private RequestSpecification addHeader(Object obj, RequestSpecification req) {
		for (Entry<String, String> entry : util.getMapVarible(obj).entrySet()) {
			req = req.header(entry.getKey(), entry.getValue());
		}
		return req;
	}

}

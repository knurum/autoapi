//Author Abdullah (abdullah.nurum@stateauto.com)
package library;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.SimpleTimeZone;

import org.apache.commons.io.FileUtils;

public class Util {
	private final String TEST_DATA_PATH = "data\\testData.xlsx";

	
	public void getEnvironmentInfo()
	{
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getLocalHost();
			String systemInfo = "ip: " +inetAddress.getHostAddress() + ", host: "+ inetAddress.getHostName() + ", system user name: "+System.getProperty("user.name");
			if (BasePage.infos.get("systemInfo") == null) {
				BasePage.infos.put("systemInfo", systemInfo);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Util() {
		if (BasePage.infos.get("createTime") == null) {
			BasePage.infos.put("createTime", Calendar.getInstance());
		}
	}

	public String getUrl(String childURL) {
		String environment = ((String[]) BasePage.infos.get("environment"))[0];
		String homeURL = ((String[]) BasePage.infos.get(environment))[0];
		return homeURL + childURL;
	}

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String, String> getMapVarible(Object var) {
		if ((var != null) && (var instanceof LinkedHashMap)) {
			return (LinkedHashMap<String, String>) var;
		}
		return new LinkedHashMap<String, String>();
	}

	public String getStringVarible(Object var) {

		if ((var != null) && (var instanceof String) && (((String) var)).trim().length() > 0) {

			return ((String) var).trim();

		}
		return null;
	}

	public PrintStream createLogStream(String filePath) {
		PrintStream logStream = null;
		try {

			logStream = new PrintStream(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return logStream;
	}

	public String createTempFile(String fileName) {
		final String tempFolderName = "temp";
		createFolder(tempFolderName);
		String tempInputFilePath = tempFolderName + "\\" + fileName + "_" + getFolderName() + ".txt";
		try {
			new File(tempInputFilePath).createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		;

		return tempInputFilePath;
	}

	public void removeFolder(String path) {
		File file = new File(path);
		if (file.exists()) {
			try {
				FileUtils.deleteDirectory(file);
			} catch (IOException e) {
			}

		}
	}

	public String getFileContent(String filePaht) {
		String content = "";
		try {
			Path path = Paths.get(filePaht);
			content = new String(Files.readAllBytes(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		content = content.trim();
		return content;
	}

	public void createReportFolder() {
		createFolder("report");
		createFolder("report\\" + getFolderName());

	}

	protected void copyTestData() {
		copyFile(TEST_DATA_PATH, getTestDataFilePath());
	}

	public String getTestDataFilePath() {
		String folderName = getFolderName();
		return "report\\" + folderName + "\\testData_" + folderName + ".xlsx";
	}

	public String getReportFilePath() {
		String folderName = getFolderName();
		return "report\\" + folderName + "\\report_" + folderName + ".html";
	}

	public String getEmailSubject() {
		return "Test Api Result " + getFolderName();
	}

	public String getEmailContent() {

		String title = "<div style=\"margin-right: auto;margin-left: auto;width: auto;\">"
				+ " <br> Dear All,<br><br> This is an auto generated test report.<br>";
		String table = "<table style=\"margin-top: 20px;border-collapse: collapse;border: 2px solid black;width: auto;\">"
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<th COLSPAN=5 style=\"border: 1px solid black;padding:5px 20px;\"><strong>API Test Report</strong></th></tr>"
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">Project Name</td>"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">"
				+ ((String[]) BasePage.infos.get("projectName"))[0] + "</td> </tr> "
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">Execution Start Time(UTC)</td>"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">" + getReadableTime() + "</td> </tr> "
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">Execution Time</td>"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">" + getExcutionTime() + "</td></tr> "
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">Environment</td>"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">"
				+ ((String[]) BasePage.infos.get("environment"))[0] + "</td></tr> "
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">Failed Test</td>"
				+ "<td  style=\"border: 1px solid black;padding:5px 20px;;background-color: red;\">"
				+ BasePage.infos.get("failedTest") + "</td></tr>"
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">Skipped Test</td>"
				+ "<td  style=\"border: 1px solid black;padding:5px 20px;;background-color: yellow;\">"
				+ BasePage.infos.get("skippedTest") + "</td></tr>"
				+ "<tr style=\"border: black solid 1px;text-align: center;word-wrap: break-word;\">"
				+ "<td style=\"border: 1px solid black;padding:5px 20px;\">Succeed Test</td>"
				+ "<td  style=\"border: 1px solid black;padding:5px 20px;background-color: green;\">"
				+ BasePage.infos.get("successTest") + "</td></tr>" + "</table>";
		String end = "</div>";
		String finalContent = title + table + end;
		return finalContent;
	}

	public boolean copyFile(String sourceFileName, String targetFileName) {
		boolean copied = false;

		File sourceFile = new File(sourceFileName);
		File targetFile = new File(targetFileName);
		if (sourceFile.exists()) {

			if (!(targetFile.exists())) {
				Path source = sourceFile.toPath();
				Path target = targetFile.toPath();
				try {
					Files.copy(source, target, REPLACE_EXISTING);
					waitFileUntilFind(targetFileName);
					copied = true;
				} catch (IOException e) {
					printStackTraceToString(e);
				}
			} else {
				copied = true;
			}

		} else {
			copied = false;
		}
		return copied;
	}

	public void waitFileUntilFind(String filePathString) {
		File f = new File(filePathString);
		for (int i = 0; i < 80; i++) {
			if (f.exists() && !f.isDirectory()) {
				break;
			} else {
				simpleWait(1000);
			}

		}
	}

	public void simpleWait(int waitingTimeMillSecond) {
		try {
			Thread.sleep(waitingTimeMillSecond);
		} catch (InterruptedException e) {
			printStackTraceToString(e);

		}
	}

	public String getStringFromInfo(String key) {

		if (BasePage.infos.get(key) != null) {
			String[] arrValue = (String[]) BasePage.infos.get(key);
			if (arrValue.length > 0) {
				return ((String[]) BasePage.infos.get(key))[0].trim();
			}
		}
		return null;
	}

	public boolean lengthIsBig(String word, int minLeng) {
		if (word == null || word.trim().length() < minLeng) {
			return false;
		}

		return true;
	}

	public void printStackTraceToString(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String sStackTrace = sw.toString();
		System.out.println(e.getMessage());
		System.out.println(sStackTrace);
	}

	public void createFolder(String folderPath) {
		File folderFile = new File(folderPath);
		if (!(folderFile.exists())) {
			folderFile.mkdir();
		}
	}

	public String getFolderName() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_EEEEE_HH_mm_ss_SSS");
		formatter.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
		String time = formatter.format(((Calendar) BasePage.infos.get("createTime")).getTime());
		return time;
	}

	public String getReadableTime() {
		{
			Calendar cal = (Calendar) BasePage.infos.get("createTime");
			return getReadableTime(cal);
		}
	}

	public String getReadableTime(Calendar cal) {
		{
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			formatter.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
			String time = formatter.format(cal.getTime());
			return time;
		}
	}

	public long getTimeDuration(Calendar startTime, Calendar endTime) {
		return endTime.getTimeInMillis() - startTime.getTimeInMillis();
	}

	public String durationToTime(long duration) {
		long hour = duration / (1000 * 60 * 60);
		long min = (duration - hour * 1000 * 60 * 60) / (1000 * 60);
		long sec = (duration - hour * 1000 * 60 * 60 - min * 1000 * 60) / (1000);
		return String.format("%d:%d:%d", hour, min, sec);
	}

	public String durationToDetailedTime(long duration) {
		long hour = duration / (1000 * 60 * 60);
		long min = (duration - hour * 1000 * 60 * 60) / (1000 * 60);
		long sec = (duration - hour * 1000 * 60 * 60 - min * 1000 * 60) / (1000);
		long milsec = (duration - hour * 1000 * 60 * 60 - min * 1000 * 60 - sec * 1000);
		return String.format("%d:%d:%d_%d", hour, min, sec, milsec);
	}

	public String getExcutionTime() {
		Calendar startTime = (Calendar) BasePage.infos.get("createTime");
		Calendar endTime = Calendar.getInstance();
		long duration = getTimeDuration(startTime, endTime);
		return durationToTime(duration);
	}

	public boolean isMapField(String field) {
		String[] fields = { "queryparam", "header", "responsesection", "cookie", "resheader" };
		field = field.trim().toLowerCase();
		for (String fieldName : fields) {
			if (field.equals(fieldName)) {
				return true;
			}
		}
		return false;
	}

	public boolean checkEnabledTest(String[] tagsInInfo, String tagsInTestCases) {
		String[] tagsCases = tagsInTestCases.split(";");
		for (String tag : tagsInInfo) {
			for (String tagCases : tagsCases) {
				if (tagCases.toLowerCase().trim().equals(tag.toLowerCase().trim())) {
					return true;
				}
			}
		}

		return false;
	}

	public String javaStringToHTMLString(String javaString) {
		String HTMLString = javaString.replace("\n", "<br>").replace("\r", "<br>");
		while (HTMLString.contains("<br><br>")) {
			HTMLString = HTMLString.replace("<br><br>", "<br>");
		}
		return HTMLString;
	}

	public String tagSaver(String input) {
		return input.replace("<", "&#60;").replace(">", "&#62;");

	}

	public enum htmlColors {
		Red, Gold, Brown, Pink, Gray, Purple, White, Orange, Green, Blue, Yellow, Black, Cyan, AliceBlue, AntiqueWhite,
		Aqua, Aquamarine, Azure, Beige, Bisque, BlanchedAlmond, BlueViolet, BurlyWood, CadetBlue, Chartreuse, Chocolate,
		Coral, CornflowerBlue, Cornsilk, Crimson, DarkBlue, DarkCyan, DarkGoldenRod, DarkGray, DarkGrey, DarkGreen,
		DarkKhaki, DarkMagenta, DarkOliveGreen, DarkOrange, DarkOrchid, DarkRed, DarkSalmon, DarkSeaGreen,
		DarkSlateBlue, DarkSlateGray, DarkSlateGrey, DarkTurquoise, DarkViolet, DeepPink, DeepSkyBlue, DimGray, DimGrey,
		DodgerBlue, FireBrick, FloralWhite, ForestGreen, Fuchsia, Gainsboro, GhostWhite, GoldenRod, Grey, GreenYellow,
		HoneyDew, HotPink, IndianRed, Indigo, Ivory, Khaki, Lavender, LavenderBlush, LawnGreen, LemonChiffon, LightBlue,
		LightCoral, LightCyan, LightGoldenRodYellow, LightGray, LightGrey, LightGreen, LightPink, LightSalmon,
		LightSeaGreen, LightSkyBlue, LightSlateGray, LightSlateGrey, LightSteelBlue, LightYellow, Lime, LimeGreen,
		Linen, Magenta, Maroon, MediumAquaMarine, MediumBlue, MediumOrchid, MediumPurple, MediumSeaGreen,
		MediumSlateBlue, MediumSpringGreen, MediumTurquoise, MediumVioletRed, MidnightBlue, MintCream, MistyRose,
		Moccasin, NavajoWhite, Navy, OldLace, Olive, OliveDrab, OrangeRed, Orchid, PaleGoldenRod, PaleGreen,
		PaleTurquoise, PaleVioletRed, PapayaWhip, PeachPuff, Peru, Plum, PowderBlue, RebeccaPurple, RosyBrown,
		RoyalBlue, SaddleBrown, Salmon, SandyBrown, SeaGreen, SeaShell, Sienna, Silver, SkyBlue, SlateBlue, SlateGray,
		SlateGrey, Snow, SpringGreen, SteelBlue, Tan, Teal, Thistle, Tomato, Turquoise, Violet, Wheat, WhiteSmoke,
		YellowGreen;
	}

	public enum contentType {
		xml, json, html, text, gif, jpeg
	}

	public contentType contentTypes(String content) {
		content = content.trim().toLowerCase();
		if (content.contains("xml")) {
			return contentType.xml;
		} else if (content.contains("html")) {
			return contentType.html;
		} else if (content.contains("text")) {
			return contentType.text;
		} else if (content.contains("gif")) {
			return contentType.gif;
		} else if (content.contains("jpeg")) {
			return contentType.jpeg;
		} else {
			return contentType.json;
		}
	}

	public int stringToNum(String string) {

		try {
			return Integer.parseInt(string.trim());
		} catch (Exception e) {
			return -1;
		}

	}
}

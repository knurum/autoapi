package library;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {
	private Util util = BasePage.util;

	public void sendEmailwithAttachment() {
		String emailTo = util.getStringFromInfo("emailReportTo");
		String emailCC = util.getStringFromInfo("emailReportCCTo");
		if (!util.lengthIsBig(emailTo, 5) && !util.lengthIsBig(emailCC, 5)) {
			return;
		}

		final String subject = util.getEmailSubject();
		final String content = util.getEmailContent();
		final String attachmentPath = util.getReportFilePath();
		final String username = "SL_GW_Smoke_Tests@stateauto.com";
		final String fileName = "report_" + util.getFolderName() + ".html";
		final String host = "smtprelay.corp.stateauto.com";
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.auth", "false");
		sendEmail(emailTo, emailCC, subject, content, attachmentPath, username, fileName, props);

		System.out.println("email sent");

	}

	private void sendEmail(String emailTo, String emailCC, String subject, String content, String attachmentPath,
			String username, String fileName, Properties props) {

		Session session = Session.getDefaultInstance(props, null);
		session.setDebug(false);
		try {
			Message message = new MimeMessage(session);
			message.saveChanges();
			message.setFrom(new InternetAddress(username));
			if (util.lengthIsBig(emailTo, 5)) {
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
			}
			if (util.lengthIsBig(emailCC, 5)) {
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(emailCC));
			}
			message.setSubject(subject);

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(content, "text/html");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(attachmentPath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}

//Author Abdullah (abdullah.nurum@stateauto.com)
package library;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {

	private final DataFormatter formatter = new DataFormatter();
	private XSSFWorkbook excelWorkBook = null;
	private final String _INFO_SHEET_NAME = "info";
	private final String _KEY_PAIR_NAME = "key_value_pair";
	private ArrayList<String> sheetNames = new ArrayList<String>();
	private Util util = BasePage.util;

	public Excel() {

		try {
			excelWorkBook = new XSSFWorkbook(new FileInputStream(util.getTestDataFilePath()));
			sheetNames = getSheetNameList();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void closeExcel() {
		try {
			FileOutputStream outStream = new FileOutputStream(util.getTestDataFilePath());
			excelWorkBook.write(outStream);
			outStream.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void writeStatus(LinkedHashMap<String, Object> testData) {
		XSSFSheet testSheet = excelWorkBook.getSheet((String) testData.get("sheet"));
		String status = (String) testData.get("status");
		XSSFRow row = null;
		int totalRow = testSheet.getLastRowNum();

		for (int i = 1; i <= totalRow; i++) {
			row = testSheet.getRow(i);
			String testNum = formatter.formatCellValue(row.getCell(0));
			if (testNum.equals(testData.get("testNum"))) {
				CellStyle cellStryle = redBackgroundBlackTextStyly(status);
				Cell statusCell = row.getCell(3);
				if (statusCell == null) {
					statusCell = row.createCell(3);
					statusCell.setCellValue(status);
					statusCell.setCellStyle(cellStryle);
				} else {
					statusCell.setCellValue(status);
					statusCell.setCellStyle(cellStryle);
				}
				break;
			}
		}

	}

	private CellStyle redBackgroundBlackTextStyly(String status) {
		String statusForCheck = status.trim().toLowerCase();
		CellStyle workbookCellStyle = excelWorkBook.createCellStyle();

		switch (statusForCheck) {
		case "fail": {
			workbookCellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
			break;
		}
		case "success": {
			workbookCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
			break;
		}

		default: {
			workbookCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
			break;
		}
		}

		workbookCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = excelWorkBook.createFont();
		font.setColor(IndexedColors.BLACK.getIndex());
		workbookCellStyle.setFont(font);
		return workbookCellStyle;
	}

	protected HashMap<String, Object> getInfoToMap() {
		HashMap<String, Object> infos = BasePage.infos;
		XSSFSheet infoSheet = excelWorkBook.getSheet(_INFO_SHEET_NAME);
		int totalRow = infoSheet.getLastRowNum();
		XSSFRow row = null;
		for (int i = 0; i <= totalRow; i++) {
			row = infoSheet.getRow(i);
			String key = formatter.formatCellValue(row.getCell(0)).trim();
			String valueString = formatter.formatCellValue(row.getCell(1)).trim();
			if (key.length() > 0 && valueString.length() > 0) {
				String[] value = valueString.split(";");
				infos.put(key, (Object) value);
			}
		}
		return infos;
	}

	private ArrayList<LinkedHashMap<String, Object>> addTestToTestCases(String sheetName, XSSFRow row,
			ArrayList<LinkedHashMap<String, Object>> testCases) {
		LinkedHashMap<String, Object> eachRowContent = new LinkedHashMap<String, Object>();

		eachRowContent.put("sheet", sheetName);
		eachRowContent.put("testNum", formatter.formatCellValue(row.getCell(0)));
		testCases.add(eachRowContent);
		return testCases;
	}

	public ArrayList<LinkedHashMap<String, Object>> getTestCases(String[] tags, String runSelection) {
		runSelection = runSelection.trim().toLowerCase();
		ArrayList<LinkedHashMap<String, Object>> testCases = new ArrayList<LinkedHashMap<String, Object>>();
		for (String sheetName : sheetNames) {
			if (sheetName.toLowerCase().startsWith("test_")) {
				XSSFSheet testSheet = excelWorkBook.getSheet(sheetName);
				XSSFRow row = null;
				int totalRow = testSheet.getLastRowNum();
				for (int i = 1; i <= totalRow; i++) {
					row = testSheet.getRow(i);
					switch (runSelection) {
					case "tagsonly": {
						String testFlag = formatter.formatCellValue(row.getCell(1));
						if (util.checkEnabledTest(tags, testFlag)) {
							testCases = addTestToTestCases(sheetName, row, testCases);
						}
						break;
					}
					case "runmodeonly": {
						String runMode = formatter.formatCellValue(row.getCell(2)).trim().toLowerCase();
						if (runMode.equals("yes")) {
							testCases = addTestToTestCases(sheetName, row, testCases);
						}
						break;
					}
					case "failedonly": {
						String status = formatter.formatCellValue(row.getCell(3)).trim().toLowerCase();
						if (status.equals("fail") || status.equals("skip")) {
							testCases = addTestToTestCases(sheetName, row, testCases);
						}
						break;
					}
					case "tagsandrunmode": {
						String testFlag = formatter.formatCellValue(row.getCell(1));
						String runMode = formatter.formatCellValue(row.getCell(2)).trim().toLowerCase();
						if (util.checkEnabledTest(tags, testFlag) && runMode.equals("yes")) {
							testCases = addTestToTestCases(sheetName, row, testCases);
						}

						break;
					}
					case "tagsandfailed": {
						String testFlag = formatter.formatCellValue(row.getCell(1));
						String status = formatter.formatCellValue(row.getCell(3)).trim().toLowerCase();
						if (util.checkEnabledTest(tags, testFlag) && (status.equals("fail") || status.equals("skip"))) {
							testCases = addTestToTestCases(sheetName, row, testCases);
						}
						break;
					}
					case "runmodeandfailed": {
						String runMode = formatter.formatCellValue(row.getCell(2)).trim().toLowerCase();
						String status = formatter.formatCellValue(row.getCell(3)).trim().toLowerCase();
						if (runMode.equals("yes") && (status.equals("fail") || status.equals("skip"))) {
							testCases = addTestToTestCases(sheetName, row, testCases);
						}

						break;
					}

					default: {
						String testFlag = formatter.formatCellValue(row.getCell(1));
						String runMode = formatter.formatCellValue(row.getCell(2)).trim().toLowerCase();
						String status = formatter.formatCellValue(row.getCell(3)).trim().toLowerCase();
						if (util.checkEnabledTest(tags, testFlag) && runMode.equals("yes")
								&& (status.equals("fail") || status.equals("skip"))) {
							testCases = addTestToTestCases(sheetName, row, testCases);
						}
						break;
					}

					}

				}
			}

		}
		return testCases;
	}

	private ArrayList<String>  getRowContent(XSSFSheet testSheet, String expectedTestNum) {
		ArrayList<String> values = new ArrayList<String>();
		int totalRow = testSheet.getLastRowNum();
		XSSFRow row = null;
		for (int i = 1; i <= totalRow; i++) {
			row = testSheet.getRow(i);
			String testNum = formatter.formatCellValue(row.getCell(0));
			if (testNum.equals(expectedTestNum)) {
				int totalColumn = row.getLastCellNum();
				for (int j = 0; j < totalColumn; j++) {
					XSSFCell cell = row.getCell(j);
					values.add(formatter.formatCellValue(cell));
				}

				break;
			}
		}
		return values;
	}

	public LinkedHashMap<String, Object> getTestData(LinkedHashMap<String, Object> testData) {
		XSSFSheet testSheet = excelWorkBook.getSheet((String) testData.get("sheet"));
		String expectedTestNum = (String) testData.get("testNum");
		ArrayList<String> rowValues = getRowContent(testSheet, expectedTestNum);
		ArrayList<String> rowTitles = getTitlesInSheet(testSheet);
		for (int i = 0; i < rowTitles.size(); i++) {
			if (rowValues.size() > i) {
				String title = rowTitles.get(i);
				String value = rowValues.get(i);
				if (util.isMapField(title)) {
					LinkedHashMap<String, String> valueMap = getKeyValuePair(value);
					testData.put(title, valueMap);
				} else {
					testData.put(title, value);
				}

			}

		}
		return testData;
	}

	private LinkedHashMap<String, String> getKeyValuePair(String stringIndex) {
		stringIndex = stringIndex.trim();
		LinkedHashMap<String, String> values = new LinkedHashMap<String, String>();
		if (stringIndex.length() == 0) {
			return values;
		}
		String[] indexs = stringIndex.split(";");
		XSSFSheet keyPairSheet = excelWorkBook.getSheet(_KEY_PAIR_NAME);
		int totalRow = keyPairSheet.getLastRowNum();
		for (String index : indexs) {
			if(index == null)continue;
			index = index.trim().toLowerCase();
			if(index.length()<1)continue;
			for (int i = 1; i <= totalRow; i++) {
				XSSFRow row = keyPairSheet.getRow(i);
				String indexNum = formatter.formatCellValue(row.getCell(0)).trim().toLowerCase();
				if (indexNum.equals(index)) {
					String key = formatter.formatCellValue(row.getCell(1)).trim();
					String value = formatter.formatCellValue(row.getCell(2)).trim();
					if (key.length() > 0 && value.length() > 0) {
						values.put(key, value);
					}
					break;
				}
			}
		}
		return values;
	}

	private ArrayList<String> getSheetNameList() {
		ArrayList<String> sheetNames = new ArrayList<String>();
		for (int i = 0; i < excelWorkBook.getNumberOfSheets(); i++) {
			sheetNames.add(excelWorkBook.getSheetName(i));
		}
		return sheetNames;
	}

	private ArrayList<String> getTitlesInSheet(XSSFSheet testSheet) {
		XSSFRow row = null;
		ArrayList<String> titles = new ArrayList<String>();
		if ((testSheet.getRow(0)) != null) {
			row = testSheet.getRow(0);
			int totalColumn = row.getLastCellNum();

			for (int i = 0; i < totalColumn; i++) {
				XSSFCell cell = row.getCell(i);
				titles.add(formatter.formatCellValue(cell));
			}

		}
		return titles;
	}
}

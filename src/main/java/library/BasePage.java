//Author Abdullah (abdullah.nurum@stateauto.com)
package library;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BasePage {

	public static HashMap<String, Object> infos = new HashMap<String, Object>();
	public static Util util = new Util();
	public Report report;
	public Excel excel;
	public static LinkedHashMap<String, Object> testData;

	@BeforeSuite
	public void beforeClass() {
		System.out.println("Folder Name: " + util.getFolderName());
		util.createReportFolder();
		util.copyTestData();
		util.getEnvironmentInfo();
		excel = new Excel();
		infos = excel.getInfoToMap();
		report = new Report();
		report.createHeader();
		infos.put("totalTest", 0);
		infos.put("failedTest", 0);
		infos.put("skippedTest", 0);
		infos.put("successTest", 0);

	}

	@AfterSuite
	public void afterSuite() {
		report.createFooter();
		excel.closeExcel();
		util.removeFolder("temp");
		Email email = new Email();
		email.sendEmailwithAttachment();

	}

}

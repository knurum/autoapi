//Author Abdullah (abdullah.nurum@stateauto.com)
package library;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListener extends BasePage implements ITestListener {



	public void onTestFailure(ITestResult arg0) {
		infos.put("failedTest", (int) (infos.get("failedTest")) + 1);
		testData.put("status", "Fail");

		if (arg0 != null && arg0.getThrowable() != null && arg0.getThrowable().getMessage() != null) {
			String message = arg0.getThrowable().getMessage();
			testData.put("failLog", message);
		}

		if (arg0 != null && arg0.getThrowable() != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			arg0.getThrowable().printStackTrace(pw);
			String sStackTrace = sw.toString();
			testData.put("failThrowable", sStackTrace);
		}

	}

	public void onTestSkipped(ITestResult arg0) {
		infos.put("skippedTest", (int) (infos.get("skippedTest")) + 1);
		testData.put("status", "Skip");
	}


	public void onTestSuccess(ITestResult arg0) {
		infos.put("successTest", (int) (infos.get("successTest")) + 1);
		testData.put("status", "Success");
	}

}

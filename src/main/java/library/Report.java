//Author Abdullah (abdullah.nurum@stateauto.com)
package library;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import library.Util.htmlColors;

public class Report {
	private BufferedWriter BW;
	private Util util = BasePage.util;

	public Report() {
		try {
			String reportPath = util.getReportFilePath();

			new File(reportPath).createNewFile();

			BW = new BufferedWriter(new FileWriter(reportPath));
		} catch (IOException e) {
			System.out.println("Could not create html file. IOE exception.");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Could not create html file. General exception.");
			e.printStackTrace();
		}

	}

	public void createHeader() {
		try {
			String htmlHeaderDom = "<!DOCTYPE html>\r\n<html>\r\n<head>"
					+ "\r\n<meta http-equiv=Content-Language content=en-us>"
					+ "\r\n<meta name=\"description\" content=\"API Test Report\">"
					+ "\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
					+ "\r\n<meta name=\"author\" content=\"Abdullah Nurum abdullah.nurum@stateauto.com\">"

					+ "\r\n<title>State Auto API Test Report</title>"
					+ "\r\n<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>"
					+ "\r\n<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>"
					+ "\r\n<style>\r\nbody{background-color:" + htmlColors.Black
					+ ";color: white;width: 848px;margin-left: auto;margin-right: auto;} table{margin-top: 20px; border-collapse: collapse; border: 2px solid white;width:844px;}"
					+ "select{margin-top: 20px;background-color: aqua;color: black;display: inline-block;vertical-align: top;} tr{text-align:center;word-wrap: break-word;} "
					+ "td, th{color:white;border: 1px solid white; overflow-wrap: break-word;word-break: break-all;} th{background-color:aqua;color:black;} .hide{display: none;} "
					+ ".hide td {text-align: left; padding-left: 5px;}"
					+ ".testID{color:red;text-decoration: underline; cursor:pointer;} #explainDiv{margin: 0 auto;text-align: center;} "
					+ "#explain{color:red;text-decoration: underline; cursor:pointer;} #explanation{display: none;} "
					+ ".Success { color: black; background-color: #53d192; } .Skip { color: black; background-color: #F4D03F; } .Fail { color: black; background-color: #f96868; }"
					+ "  #image { width: 300px; } img { width:100%; height: auto; }  #piechart{margin-left: 30px; margin-top: 5px;display:inline-block;vertical-align:top;}\r\n</style>"
					+ "\r\n</head>\r\n<body>\r\n<header>"
					+ "<div id=\"image\"><img src=\"https://www.stateauto.com/public/files/StateAuto_logo.jpg\" alt=\"State Auto Insurance\"></div>"
					+ "</header>" + "<table id='titleTable'>" + "\r\n<tr><th COLSPAN=5><p><strong>"
					+ ((String[]) BasePage.infos.get("projectName"))[0] + "</strong></p></th></tr>"
					+ "<tr><td><p>Execution Start Time</p></td><td id=\"executionStartingTime\">"
					+ util.getReadableTime() + "</td></tr>"
					+ "<tr><td>Execution Time</td><td id=\"executionTime\"></td></tr>" + "<tr><td>Environment</td><td>"
					+ ((String[]) BasePage.infos.get("environment"))[0] + "</td></tr>" + "<tr><td>System Info</td><td>"
					+ ((String) BasePage.infos.get("systemInfo")) + "</td></tr>"
					+ "<tr><td>Failed Test</td><td class=\"Fail\" id=\"Fail\"></td></tr>"
					+ "<tr><td>Skipped Test</td><td id=\"Skip\" class=\"Skip\"></td></tr>"
					+ "<tr><td>Succeed Test</td><td class=\"Success\" id=\"Success\"></td></tr>" + "\r\n</table>\r\n"
					+ " <div> <select id=\"selectTest\"> <option value=\"all\">All</option> <option value=\"failed\">Failed</option> <option value=\"skipped\">Skipped</option> <option value=\"successed\">Successed</option> <option value=\"failedAndSkipped\">Failed And Skipped</option> <option value=\"successedAndFailed\">Successed And Failed</option> <option value=\"successedAndSkipped\">Successed And Skipped</option> </select>"
					+ "<div id=\"piechart\"></div>" + "</div>" + "<table id='reportTable'>\r\n"
					+ "<tr><th width=120px>Test Case ID</th><th>Scenario Description</th><th width=65px>Status</th></tr>";

			writeline(htmlHeaderDom);

		} catch (Exception e) {

		}
	}

	public void createFooter() {
		writeline("</table>\r\n");
		String htmlFooterDom = "<div id='explainDiv' style='margin-left:10%;margin-right:10%;'>\r\n<P id='explain'>Help</p><p id='explanation'>"
				+ "Click Test Case ID can check steps." + "</p>" + "\r\n</div>\r\n</body></html>";
		writeline(htmlFooterDom);

		writeline(
				"<script>function showhidden(className){if ($('.' + className +\".hide\").is(\":visible\")) {$('.hide').hide();} else { $('.hide').hide(); $('.' + className).show(1000); } } $('#explain').click(function () { $('#explanation').toggle(1000); window.scrollTo(0, document.body.scrollHeight); }); </script>");

		writeline("<script>var date = new Date('" + util.getReadableTime()
				+ " UTC');document.getElementById('executionStartingTime').innerHTML = date.toString();</script>");

		writeline("<script>document.getElementById('Fail').innerHTML = \"" + getResultValue("failedTest")
				+ "\";</script>");
		writeline("<script>document.getElementById('Skip').innerHTML = \"" + getResultValue("skippedTest")
				+ "\";</script>");
		writeline("<script>document.getElementById('Success').innerHTML = \"" + getResultValue("successTest")
				+ "\";</script>");
		writeline("<script>document.getElementById('executionTime').innerHTML = \"" + util.getExcutionTime()
				+ "\";</script>");
		writeline(
				"<script> $('#selectTest').change(function() { const selection = $(this).val(); $('.hide').hide(); switch(selection) { case \"all\": { $('.parFail').show(1000); $('.parSkip').show(1000); $('.parSuccess').show(1000); break; } case \"successed\": { $('.parFail').hide(); $('.parSkip').hide(); $('.parSuccess').show(1000); break; } case \"failed\": { $('.parFail').show(1000); $('.parSkip').hide(); $('.parSuccess').hide(); break; } case \"skipped\": { $('.parFail').hide(); $('.parSkip').show(1000); $('.parSuccess').hide(); break; } case \"successedAndFailed\": { $('.parFail').show(1000); $('.parSkip').hide(); $('.parSuccess').show(1000); break; } case \"successedAndSkipped\": { $('.parFail').hide(); $('.parSkip').show(1000); $('.parSuccess').show(1000); break; } case \"failedAndSkipped\": { $('.parFail').show(1000); $('.parSkip').show(1000); $('.parSuccess').hide(); break; } default: { $('.parFail').show(1000); $('.parSkip').show(1000); $('.parSuccess').show(1000); break; } } }); </script>");

		String pie = "<script type=\"text/javascript\">\r\n" + "  // Load google charts\r\n"
				+ "  google.charts.load('current', { 'packages': ['corechart'] });\r\n"
				+ "  google.charts.setOnLoadCallback(drawChart);\r\n" + "\r\n"
				+ "  // Draw the chart and set the chart values\r\n" + "  function drawChart() {\r\n"
				+ "    var data = google.visualization.arrayToDataTable([\r\n"
				+ "      ['Task', 'result percentage'],\r\n" + "      ['Fail', "
				+ (int) BasePage.infos.get("failedTest") + "],\r\n" + "      ['Success', "
				+ (int) BasePage.infos.get("successTest") + "],\r\n" + "      ['Skip', "
				+ (int) BasePage.infos.get("skippedTest") + "],\r\n" + "\r\n" + "    ]);\r\n" + "\r\n"
				+ "    // Optional; add a title and set the width and height of the chart\r\n"
				+ "    var options = {\r\n" + "      'width': 120, 'height': 120,\r\n"
				+ "      colors: ['#f96868', '#53d192', '#F4D03F'],\r\n" + "      pieSliceTextStyle: {\r\n"
				+ "        color: 'black'\r\n" + "      },\r\n" + "      ieHole: 0.4,\r\n"
				+ "      backgroundColor: 'black',\r\n" + "      legend: 'none',\r\n"
				+ "      chartArea:{left:0,top:0,width:'100%',height:'100%'}\r\n" + "    };\r\n" + "\r\n"
				+ "    // Display the chart inside the <div> element with id=\"piechart\"\r\n"
				+ "    var chart = new google.visualization.PieChart(document.getElementById('piechart'));\r\n"
				+ "    chart.draw(data, options);\r\n" + "  }\r\n" + "</script>";
		writeline(pie);
		closeBW();

	}

	private String getResultValue(String key) {
		int total = (int) BasePage.infos.get("failedTest") + (int) BasePage.infos.get("skippedTest")
				+ (int) BasePage.infos.get("successTest");
		if (total == 0)
			total = 1;
		int num = (int) BasePage.infos.get(key);
		int percentage = num * 100 / total;

		return num + " (" + percentage + " %)";
	}

	private void writeMap(String title, Object testDataObject, String trClass) {
		for (Entry<String, String> entry : util.getMapVarible(testDataObject).entrySet()) {
			String content = "<tr class=\"hide " + trClass + "\"><td>" + title + "</td><td>Key: " + entry.getKey()
					+ ", value: " + entry.getValue() + "</td><td class=\"\"></td></tr>";
			writelineWithChange(content);
		}
	}

	private void writeString(String title, Object testDataObject, String trClass) {
		if (util.getStringVarible(testDataObject) != null) {
			String content = util.tagSaver(util.getStringVarible(testDataObject));
			content = "<tr class=\"hide " + trClass + "\"><td>" + title + "</td><td>" + content
					+ "</td><td class=\"\"></td></tr>";
			writelineWithChange(content);
		}
	}

	public void writeRecord(LinkedHashMap<String, Object> testData) {
		String testSheet = (String) testData.get("sheet");
		String sheetTestNum = (String) testData.get("testNum");

		int testNum = (int) (BasePage.infos.get("totalTest"));
		String testName = (util.getStringVarible(testData.get("name")) != null)
				? (util.getStringVarible(testData.get("name")))
				: (util.getUrl((util.getStringVarible(testData.get("url")))));
		testName = testSheet +", "+ sheetTestNum +", "+ testName;
		String status = ((String) (testData.get("status")) != null) ? (String) (testData.get("status")) : "null";
		String trClass = "trclass" + testNum;
		String firstLine = "<tr class=\"show " + trClass + " par" + status
				+ "\"><td class=\"testID\"+ onclick=\"showhidden('" + trClass + "')\">Test " + testNum + "</td><td>"
				+ testName + "</td><td class=\"" + status + "\">" + status + "</td></tr>";
		writelineWithChange(firstLine);
		String url = "<tr class=\"hide " + trClass + "\"><td>URL</td><td>"
				+ util.getUrl((util.getStringVarible(testData.get("url")))) + "</td><td class=\"\"></td></tr>";
		writelineWithChange(url);

		if (testData.get("requestStartTime") != null && testData.get("requestFinishedTime") != null) {
			String duration = util.durationToDetailedTime(util.getTimeDuration(
					(Calendar) testData.get("requestStartTime"), (Calendar) testData.get("requestFinishedTime")));
			duration = "<tr class=\"hide " + trClass + "\"><td>Response Time</td><td>" + duration
					+ "</td><td class=\"\"></td></tr>";
			writelineWithChange(duration);
		}
		writeString("Method", testData.get("method"), trClass);
		writeMap("Query Param", testData.get("queryParam"), trClass);
		writeMap("Request Header", testData.get("header"), trClass);
		writeString("Request Body", testData.get("requestBody"), trClass);
		writeString("Expected Status Code", testData.get("statusCode"), trClass);
		writeMap("Expected Cookie", testData.get("cookie"), trClass);
		writeMap("Expected Response Header", testData.get("resHeader"), trClass);
		writeString("Expected Response Body", testData.get("responseBody"), trClass);
		writeMap("Expected Response Section", testData.get("responseSection"), trClass);
		writeString("Expected Response Content Type", testData.get("responseContentType"), trClass);
		writeString("Request Logging Filter", testData.get("requestObject"), trClass);
		writeString("Response Logging Filter", testData.get("responseObject"), trClass);
		writeString("Fail Log", testData.get("failLog"), trClass);
		writeString("Throwable", testData.get("failThrowable"), trClass);

	}

	private void closeBW() {
		try {
			BW.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeline(String lineContent) {
		try {
			BW.write(lineContent);
			BW.newLine();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void writelineWithChange(String lineContent) {
		lineContent = util.javaStringToHTMLString(lineContent);
		try {
			BW.write(lineContent);
			BW.newLine();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
